package pl.edu.uwm.wmii.gardaspiotr.labolatorium10.ArrayUtil;

import java.time.LocalDate;

public class TestArrayUtil {
    public static void main(String[] args) {
        Integer [] ssorted = {7,6,5,5,4,3,5,2,1};
        Integer [] sorted = {2,2,4,5,6,7};
        Integer [] notSorted = {7,6,5,5,4,3,5,2,1};
        System.out.println(ArrayUtil.isSorted(sorted));
        System.out.println(ArrayUtil.isSorted(notSorted));
        LocalDate[] sortedDates =
                {
                LocalDate.of(1299,6,12),
                LocalDate.of(1753,2,4),
                LocalDate.of(1900,1,17),
                LocalDate.of(1937,6,2)
                };
        LocalDate[] notSortedDates =
                {
                LocalDate.of(1299,6,12),
                LocalDate.of(2038,2,4),
                LocalDate.of(1753,1,17),
                LocalDate.of(1937,6,2)
                };
        LocalDate[] notSortedDatesV2 =
                {
                        LocalDate.of(1299,6,12),
                        LocalDate.of(2038,2,4),
                        LocalDate.of(1753,1,17),
                        LocalDate.of(1937,6,2)
                };
        System.out.println(ArrayUtil.isSorted(sortedDates));
        System.out.println(ArrayUtil.isSorted(notSortedDates));
        ///bin search
        System.out.println(ArrayUtil.binSearch(sorted, 4 ));
        System.out.println(ArrayUtil.binSearch(sorted, 50 ));
        System.out.println(ArrayUtil.binSearch(notSortedDates, LocalDate.of(1753,1,17) ));
        System.out.println(ArrayUtil.binSearch(notSortedDates, LocalDate.of(1223,1,17) ));
        ///selection sort
        ArrayUtil.selectionSort(notSorted);
        for(Integer number:notSorted)
        {
            System.out.print(number);
        }
        System.out.println();

        ArrayUtil.selectionSort(notSortedDates);
        for(LocalDate date:notSortedDates)
        {
            System.out.print(date.toString() + ' ');
        }
        System.out.println();


        ArrayUtil.mergeSort(ssorted);

        for(int number:ssorted)
        {
            System.out.print(number);
        }
        System.out.println();

        ArrayUtil.mergeSort(notSortedDatesV2);

        for(LocalDate date:notSortedDatesV2)
        {
            System.out.print(date.toString() + ' ');
        }
        System.out.println();

    }
}
