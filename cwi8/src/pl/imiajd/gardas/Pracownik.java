package pl.imiajd.gardas;

import pl.imiajd.gardas.Osoba;

import java.time.LocalDate;

public class Pracownik extends Osoba {
    public Pracownik(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec, double pobory, LocalDate dataZatrudnienia) {
        super(nazwisko, imiona, dataUrodzenia, plec);
        this.pobory = pobory;
        this.dataZatrudnienia = dataZatrudnienia;
    }

    public double getPobory() {
        return pobory;
    }

    public LocalDate getDataZadrudnienia() {
        return dataZatrudnienia;
    }

    public String getOpis() {
        StringBuffer Imiona = new StringBuffer();
        for (String x : getImiona()) {
            Imiona.append(x + ' ');
        }
        return String.format("student %s %s plec %s z pensją %.2f zł urodzony %s , zatrudniony %s",
                Imiona, getNazwisko(), getPlec() == true ? "mezczyzna" : "kobieta",
                pobory, getDataUrodzenia().toString(),
                getDataZadrudnienia().toString());
    }

    private double pobory;

    private LocalDate dataZatrudnienia;
}
