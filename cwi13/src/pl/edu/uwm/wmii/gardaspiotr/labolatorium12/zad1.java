package pl.edu.uwm.wmii.gardaspiotr.labolatorium12;

import java.util.PriorityQueue;
import java.util.Scanner;

public class zad1 {

    PriorityQueue<Zadania> q = new PriorityQueue<Zadania>();
    Scanner scan = new Scanner(System.in);
    boolean menu()
    {
        System.out.println("komendy- dodaj priorytet opis -> dodaje priorytet opis," +
                " nastepne -> wypisuje oraz zdejmuje zadanie o najmniejszym priortytecie" +
                ", wszystko inne ->exit");
        String Choice = new String(scan.nextLine());
        if(Choice.equals("dodaj priorytet opis"))
        {
            int priorytet = scan.nextInt();
            if(scan.hasNext()) {scan.nextLine();}
            String Opis = new String(scan.nextLine());
            q.add(new Zadania(priorytet, Opis));
            return true;
        }
        else if(Choice.equals("nastepne"))
        {
            if(q.isEmpty())
            {
                System.out.println("kolejka jest pusta" );
                return true;
            }
            System.out.println("zdjeto zadanie " + q.peek().getOpis() + " o priorytecie " + q.poll().getPriorytet() );
            return true;
        }

        System.out.println(Choice);
        System.out.println("koniec");
        return false;

    }

    public static void main(String[] args)
    {
        zad1 zad = new zad1();
       while(zad.menu());


    }
    class Zadania implements Comparable
    {
        int Priorytet;
        String Opis;
        Zadania(int Priorytet, String Opis)
        {
            this.Priorytet = Priorytet;
            this.Opis = Opis;
        }
        int getPriorytet() {return Priorytet;}
        String getOpis() {return Opis;}

        @Override
        public int compareTo(Object o) {
            return Priorytet - ((Zadania)o).getPriorytet();
        }
    }

}
