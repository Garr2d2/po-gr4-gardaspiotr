package pl.edu.uwm.wmii.gardaspiotr.labolatorium02;

import java.util.Random;

public class Zad2_b {
    public Zad2_b()
    {
        Rand = new Random();
    }
    private static Random Rand;

    public static void generuj(int Arr[], int n, int Min, int Max)
    {
        for(int i = 0; i<n; i++)
        {
            Arr[i] = (Math.abs((Rand.nextInt()%(Math.abs(Min)+Math.abs(Max)))))-Math.abs(Min);
        }
    }
    public static int IleDodatnich(int tab[])
    {
        int result = 0;
        for(int zmienna : tab)
        {
            if(zmienna>0)
            {
                result++;
            }
        }
        return result;
    }
    public static int IleUjemnych(int tab[])
    {
        int result = 0;
        for(int zmienna : tab)
        {
            if(zmienna < 0)
            {
                result++;
            }
        }
        return result;
    }
    public static int IleZerowych(int tab[])
    {
        int result = 0;
        for(int zmienna : tab)
        {
            if(zmienna == 0)
            {
                result++;
            }
        }
        return result;
    }
    //////////////main
    public static void main(String[] args) {
        int n = 10;

        int Arr[] = new int[n];
        Zad2_b funb = new Zad2_b();
        funb.generuj(Arr, n, (-999), 999);
        int IleD = funb.IleDodatnich(Arr);
        int IleU = funb.IleUjemnych(Arr);
        int IleZ = funb.IleZerowych(Arr);
        System.out.println(IleD);
        System.out.println(IleU);
        System.out.println(IleZ);

    }
}
