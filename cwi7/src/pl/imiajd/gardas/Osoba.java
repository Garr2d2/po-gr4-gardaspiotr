package pl.imiajd.gardas;

public class Osoba {
    private String nazwisko = null;
    private String rokUrodzenia = null;
    public Osoba(String naz, String rok)
    {
        nazwisko = naz;
        rokUrodzenia = rok;
    }
    public String getNazwisko() {return nazwisko;}
    public String getRokUrodzenia() {return rokUrodzenia;}
    public String toString()
    {
        return (String)("nazwisko: " +nazwisko + " " + "Rok urodzenia: " + rokUrodzenia);
    }
}
