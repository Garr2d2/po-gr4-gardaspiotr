package pl.edu.uwm.wmii.gardaspiotr.labolatorium11;

import java.util.Arrays;
import java.util.LinkedList;

public class zad4 {
    public  static <T> void odwroc(LinkedList<T> lista)
    {
        for(int i = 0,j = lista.size()-1; i<j; i++, j--)
        {
            T temp = lista.get(i);
            lista.set(i, lista.get(j));
            lista.set(j, temp);
        }

    }

    public static void main(String[] args) {
        LinkedList<Integer> pracownicy = new LinkedList<Integer>(Arrays.asList(1, 2,3,4));
        for (Integer x : pracownicy) {
            System.out.println(x);
        }
        odwroc(pracownicy);
        System.out.println();
        for (Integer x : pracownicy) {
            System.out.println(x);
        }
    }
}
