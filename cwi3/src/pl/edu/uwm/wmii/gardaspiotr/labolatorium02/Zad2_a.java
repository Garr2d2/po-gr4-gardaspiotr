package pl.edu.uwm.wmii.gardaspiotr.labolatorium02;

import java.util.Random;
import java.util.Scanner;

public class Zad2_a {
    public Zad2_a()
    {
        Rand = new Random();
    }
    private static Random Rand;

    public static void generuj(int Arr[], int n, int Min, int Max)
    {
        for(int i = 0; i<n; i++)
        {
            Arr[i] = (Math.abs((Rand.nextInt()%(Math.abs(Min)+Math.abs(Max)))))-Math.abs(Min);
        }
    }
    public static int IleNieparzystych(int tab[])
    {
        int result = 0;
        for(int zmienna : tab)
        {
            if(Math.abs(zmienna)%2 == 1)
            {
                result++;
            }
        }
        return result;
    }
    public static int IleParzystych(int tab[])
    {
        int result = 0;
        for(int zmienna : tab)
        {
            if(Math.abs(zmienna)%2 == 0)
            {
                result++;
            }
        }
        return result;
    }
    //////////////main
    public static void main(String[] args) {
        int n = 10;

        int Arr[] = new int[n];
        Zad2_a fun = new Zad2_a();
        fun.generuj(Arr, n, (-999), 999);
        int IleN = fun.IleNieparzystych(Arr);
        int IleP = fun.IleParzystych(Arr);
        System.out.println(IleN);
        System.out.println(IleP);

    }
}
