package pl.edu.uwm.wmii.gardaspiotr.labolatorium13;

import pl.imiajd.gardas.Ksiazka;

import java.util.Arrays;
import java.util.LinkedList;

public class Zadanie2
{
    public static void redukuj(LinkedList<String> books, int n)
    {
        if(n<=0 | n > books.size())
        {
            return;
        }
        for(int i = n-1; i<books.size(); i+=n-1)
        {
            books.remove(i);
        }
    }

    public static void main(String[] args)
    {
        LinkedList<String> books = new LinkedList<String>(
                Arrays.asList(new Ksiazka("jaś", "email1", 'm', "tytul1",8.99).toString(),
                new Ksiazka("jaś", "email1", 'm', "tytul2",5.99).toString(),
                new Ksiazka("malgosia", "email2", 'k', "tytul3",2.99).toString(),
                new Ksiazka("karolina", "email4", 'k', "tytul4",5.99).toString(),
                new Ksiazka("jaś", "email1", 'm', "tytul5",8.99).toString(),
                new Ksiazka("jaś", "email1", 'm', "tytul6",5.99).toString(),
                new Ksiazka("malgosia", "email2", 'k', "tytul7",2.99).toString(),
                new Ksiazka("karolina", "email4", 'k', "tytul8",5.99).toString()
                ));

        for(String x : books)
        {
            System.out.println(x);
        }
        redukuj(books, 2);

        System.out.println();

        for(String x : books)
        {
            System.out.println(x);
        }

        System.out.println();
        System.out.println();

        Ksiazka.redukuj(books, 4); //same method but written inside "Ksiazka" class because i didnt understand correctly if this have to be standalone method or part of "ksiazka" interface

        for(String x : books)
        {
            System.out.println(x);
        }

    }
}
