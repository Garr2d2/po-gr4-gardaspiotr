package pl.imiajd.gardas;

import java.time.LocalDate;
import java.util.Objects;

import static java.lang.Integer.signum;

public class Osoba implements Cloneable, Comparable
{
    public Osoba(String nazwisko, LocalDate dataUrodzenia)
    {
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
    }
    private String nazwisko;
    private LocalDate dataUrodzenia;

    public String getNazwisko() {return nazwisko;}
    public LocalDate getDataUrodzenia(){return dataUrodzenia;}

    @Override
    public String toString()
    {
        return String.format("%s [%s], [%s]",this.getClass().getSimpleName(), nazwisko, dataUrodzenia.toString());
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Osoba osoba = (Osoba) o;
        return Objects.equals(nazwisko, osoba.nazwisko) &&
                Objects.equals(dataUrodzenia, osoba.dataUrodzenia);
    }


    @Override
    public int compareTo(Object o)
    {
        if(o == null)
        {
            throw new NullPointerException();
        }
        if(getClass() != o.getClass())
        {
            throw new ClassCastException();
        }

        return signum(this.toString().compareTo(o.toString()));//class names had to be the same (exception else)
                                                        // so we are comparing two same styles strings where first is the name and then is date
    }


}
