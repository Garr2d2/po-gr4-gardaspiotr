package pl.edu.uwm.wmii.gardaspiotr.labolatorium11;

import java.util.*;

public class zad8 {
    static <E extends Iterable> void print(E arg)
    {
        Iterator<E> iter = (arg.iterator());
        while(iter.hasNext())
        {
            System.out.print(iter.next() + (iter.hasNext()? ", " :""));
        }
    }
    public static void main(String[] args) {
        ArrayList<String> s1 = new ArrayList<String>(Arrays.asList("jeden", "dwa", "trzy"));
        LinkedList<String> pracownicy = new LinkedList<String>(Arrays.asList("prac1", "prac2", "prac3", "prac4","prac5", "prac6", "prac7", "prac8" ));
        Stack<Integer> stack = new Stack<Integer>();
        int number = 2015;
        for(int i = 1 ; number > 0; i++  )
        {
            stack.push(number%10);
            number/=10;
        }
        //int [] tab = {1,2,3};
        //print(tab);
        print(s1);
        System.out.println();
        print(pracownicy);
        System.out.println();
        print(stack);
        System.out.println();
    }
}
