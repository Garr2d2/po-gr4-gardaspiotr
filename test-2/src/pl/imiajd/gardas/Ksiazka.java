package pl.imiajd.gardas;

import java.util.LinkedList;

public class Ksiazka implements Cloneable, Comparable
{
    String tytul;
    Autor autor;
    double cena;

    public Ksiazka(String nazwa, String email, char plec, String tytul, double cena)
    {
        this.autor = new Autor(nazwa, email, plec);
        this.tytul = tytul;
        this.cena = cena;
    }
    @Override
    public int compareTo(Object o) {
        if (this.autor.compareTo(((Ksiazka) o).autor) != 0) {
            return this.autor.compareTo(((Ksiazka) o).autor);
        }
        if (this.tytul.compareTo(((Ksiazka) o).tytul) != 0) {
            return this.tytul.compareTo(((Ksiazka) o).tytul);
        }

        return (int) Math.signum((this.cena - ((Ksiazka) o).cena));
    }

    @Override
    public String toString() {
        return autor.toString() + ", tytul = " + tytul + ", cena = " +  cena+ " ";
    }

    public static void redukuj(LinkedList<String> books, int n) {
        if (n <= 0 | n > books.size()) {
            return;
        }
        for (int i = n - 1; i < books.size(); i += n - 1) {
            books.remove(i);
        }
    }
}
