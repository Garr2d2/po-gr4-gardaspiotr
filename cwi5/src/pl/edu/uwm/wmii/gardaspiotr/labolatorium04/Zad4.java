package pl.edu.uwm.wmii.gardaspiotr.labolatorium04;

import java.util.ArrayList;
import java.util.Arrays;

public class Zad4 {
    public static ArrayList<Integer> reversed(ArrayList<Integer> a)
    {
        ArrayList<Integer> result = new ArrayList<Integer>();
        for(int i = a.size()-1; i >= 0; i--)
        {
            result.add(a.get(i));
        }
        return result;
    }
    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<Integer>(Arrays.asList(1,2,3,4));
        System.out.println(reversed(a));
    }
}
