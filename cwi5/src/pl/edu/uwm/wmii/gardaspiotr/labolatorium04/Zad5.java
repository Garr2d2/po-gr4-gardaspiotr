package pl.edu.uwm.wmii.gardaspiotr.labolatorium04;

import java.util.ArrayList;
import java.util.Arrays;

public class Zad5 {
    public static void reversed(ArrayList<Integer> a)
    {
        for(int j = a.size()-1, i = 0; i<j; i++, j--)
        {
            int temp = a.get(i);
            a.set(i, a.get(j));
            a.set(j, temp);
        }

    }
    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<Integer>(Arrays.asList(1,2,3,4));
        reversed(a);
        System.out.println(a);
    }
}
