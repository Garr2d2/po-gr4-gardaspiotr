package pl.edu.uwm.wmii.gardaspiotr.labolatorium09;

import pl.imiajd.gardas.Student;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;

public class TestStudent {
    public static void main(String[] args) {
        ArrayList<Student> grupa = new ArrayList<Student>(Arrays.asList(new Student("Gardas", LocalDate.of(1996,12,2),4.79),
                new Student("Jasiewicz", LocalDate.of(1999,2,2), 4.0),
                new Student("Adasiewicz", LocalDate.of(1999,2,2),3.78),
                new Student("Adasiewicz", LocalDate.of(1998,2,2),5.0),
                new Student("Adasiewicz", LocalDate.of(1998,2,2),4.0),
                new Student("Malgosiewicz", LocalDate.of(1999,2,2),3.0)));

        for(Student x : grupa)
        {
            System.out.println(x.toString());
        }

        grupa.sort(Student::compareTo);
        System.out.println();

        for(Student x : grupa)
        {
            System.out.println(x.toString());
        }

    }
}
