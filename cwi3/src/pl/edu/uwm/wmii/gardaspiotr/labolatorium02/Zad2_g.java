package pl.edu.uwm.wmii.gardaspiotr.labolatorium02;

import java.util.Random;

public class Zad2_g {
    public Zad2_g()
    {
        Rand = new Random();
    }
    private static Random Rand;

    public static void generuj(int Arr[], int n, int Min, int Max)
    {
        for(int i = 0; i<n; i++)
        {
            Arr[i] = (Math.abs((Rand.nextInt()%(Math.abs(Min)+Math.abs(Max)))))-Math.abs(Min);
        }
    }
    public static void OdwrocFragment(int Arr[], int lewy, int prawy)
    {
        if(prawy<lewy)
        {
            int temp = prawy;
            prawy = lewy;
            lewy = prawy;
        }
        for(int i  = lewy, j = prawy; i<j;i++,j--)
        {
            int temp = Arr[i];
            Arr[i]= Arr[j];
            Arr[j] = temp;
        }
    }

    //////////////main
    public static void main(String[] args) {
        int n = 10;
        Zad2_g func = new Zad2_g();
        int Arr[] = new int[n];

        func.generuj(Arr, n, (-999), 999);
        int lewy = 5;
        int prawy = 9;
        func.OdwrocFragment(Arr,lewy, prawy);



    }
}
