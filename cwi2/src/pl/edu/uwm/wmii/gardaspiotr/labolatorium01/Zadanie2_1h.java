package pl.edu.uwm.wmii.gardaspiotr.labolatorium01;

import java.util.Scanner;

public class Zadanie2_1h {
    public static void main(String[] args) {

        Scanner Scn = new Scanner(System.in);
        System.out.println("enter number of cases");
        int n = Scn.nextInt();
        int Arr[] = new int[n];
        int count = 0;
        for (int i = 0; i < n; i++) {
            System.out.print("enter next number: ");
            Arr[i] = Scn.nextInt();
        }

        for(int i = 0; i<n ; i++)
        {

            if(Math.abs(Arr[i]) < Math.pow(i,2))
            {
                count++;
            }
        }
        System.out.println("result " + count);
    }
}
