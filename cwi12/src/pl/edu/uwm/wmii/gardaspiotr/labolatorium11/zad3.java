package pl.edu.uwm.wmii.gardaspiotr.labolatorium11;

import java.util.Arrays;
import java.util.LinkedList;

public class zad3 {
    public  static  void odwroc(LinkedList<String> lista)
    {
        for(int i = 0,j = lista.size()-1; i<j; i++, j--)
        {
            String temp = lista.get(i);
            lista.set(i, lista.get(j));
            lista.set(j, temp);
        }

    }

    public static void main(String[] args) {
        LinkedList<String> pracownicy = new LinkedList<String>(Arrays.asList("1", "2","3","4"));
        for (String x : pracownicy) {
            System.out.println(x);
        }
        odwroc(pracownicy);
        System.out.println();
        for (String x : pracownicy) {
            System.out.println(x);
        }
    }
}
