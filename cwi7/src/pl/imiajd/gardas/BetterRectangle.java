package pl.imiajd.gardas;
import java.awt.Rectangle;
public class BetterRectangle extends Rectangle{
    public BetterRectangle(int x, int y, int he, int wi)
    {
        super();
        setLocation(x,y);
        setSize(wi, he);
    }
    public double getPerimeter()
    {
        return 2 * getHeight()  +  2 * getWidth();
    }
    public double GetArea()
    {
        return getHeight() * getWidth();
    }
}
