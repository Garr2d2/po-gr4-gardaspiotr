package pl.edu.uwm.wmii.gardaspiotr.labolatorium13;

class Autor implements Cloneable, Comparable
{
    String nazwa;
    String email;
    char plec;
    void setNazwa(String nazwa) {this.nazwa = nazwa;}
    String getNazwa(){return nazwa;}

    void setEmail(String email) {this.email = email;}
    String getEmail(){return email;}

    void setPlec(char plec) {this.plec = plec;}
    char getPlec(){return plec;}

    @Override
    public int compareTo(Object o) {
        if(this.getNazwa().compareTo(((Autor)o).getNazwa()) != 0)
        {
            return this.getNazwa().compareTo(((Autor)o).getNazwa());
        }
        return (int)(this.getPlec() - ((Autor)o).getPlec());
    }

    @Override
    public String toString() {
        return "Autor [nazwisko = " + nazwa +", email = " +email+", plec = " + plec + "]";
    }



    class Ksiazka implements Cloneable, Comparable
    {
        String tytul;
        Autor autor;
        double cena;

        @Override
        public int compareTo(Object o) {
            if(this.autor.compareTo(((Ksiazka)o).autor) != 0)
            {
                return this.autor.compareTo(((Ksiazka)o).autor);
            }
            if(this.tytul.compareTo(((Ksiazka)o).tytul) != 0)
            {
                return this.tytul.compareTo(((Ksiazka)o).tytul);
            }

            return (int)java.lang.Math.signum((this.cena - ((Ksiazka)o).cena));
        }
    }
}
