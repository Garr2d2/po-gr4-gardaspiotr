package pl.edu.uwm.wmii.gardaspiotr.labolatorium05;

public class RachunekBankowy {

    RachunekBankowy()
    {
        saldo = 0;
    }

    RachunekBankowy(double saldo)
    {
        this.saldo = saldo;
    }
    public String toString()
    {
        return String.valueOf(saldo);
    }
    static double rocznaStopaProcentowa;
    private double saldo;
    double obliczMiesieczneOdsetki()
    {
        double odsetki = saldo * rocznaStopaProcentowa / 12;
        saldo += odsetki;
        return odsetki;
    }
    static void setRocznaStopaProcentowa(double newRocznaStopa)
    {
        rocznaStopaProcentowa = newRocznaStopa;
    }



}
