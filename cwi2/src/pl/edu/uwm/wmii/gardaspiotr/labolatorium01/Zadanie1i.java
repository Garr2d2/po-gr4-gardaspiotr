package pl.edu.uwm.wmii.gardaspiotr.labolatorium01;

import java.util.Scanner;

public class Zadanie1i {
    public static void main(String[] args) {
        Scanner Scn = new Scanner(System.in);
        System.out.println("enter number of cases");
        int n = Scn.nextInt();
        double result = 0;
        for (int i =0; i<n; i++)
        {
            double x;
            int fac = 1;
            System.out.print("enter next number: ");
            x = Scn.nextDouble();
            for(int j = i+1;j>0;j--)
            {
                fac*=j;
            }

            result+=Math.pow(-1,i+1)* x/fac;
        }
        System.out.println("result " + result);
    }
}
