package pl.edu.uwm.wmii.gardaspiotr.labolatorium02;

import java.util.Random;

public class Zad2_d {
    public Zad2_d()
    {
        Rand = new Random();
    }
    private static Random Rand;

    public static void generuj(int Arr[], int n, int Min, int Max)
    {
        for(int i = 0; i<n; i++)
        {
            Arr[i] = (Math.abs((Rand.nextInt()%(Math.abs(Min)+Math.abs(Max)))))-Math.abs(Min);
        }
    }
    public static int SumaPozytywnych(int Arr[])
    {
        int Count = 0;

        for(int i = 0; i<Arr.length;i++)
        {
            if(Arr[i]>0)
            {
                Count+=Arr[i];
            }

        }
        return Count;
    }
    public static int SumaNegatywnych(int Arr[])
    {
        int Count = 0;

        for(int i = 0; i<Arr.length;i++)
        {
            if(Arr[i]<0)
            {
                Count+=Arr[i];
            }

        }
        return Count;
    }

    //////////////main
    public static void main(String[] args) {
        int n = 10;
        Zad2_d func = new Zad2_d();
        int Arr[] = new int[n];

        func.generuj(Arr, n, (-999), 999);
        int IleP = func.SumaPozytywnych(Arr);
        int IleN = func.SumaNegatywnych(Arr);

        System.out.println(IleP);
        System.out.println(IleN);


    }
}
