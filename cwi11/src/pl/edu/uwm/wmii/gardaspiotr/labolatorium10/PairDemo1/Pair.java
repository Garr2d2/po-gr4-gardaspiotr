package pl.edu.uwm.wmii.gardaspiotr.labolatorium10.PairDemo1;
public class Pair<T> {

    public Pair() {
        first = null;
        second = null;
    }

    public Pair (T first, T second) {
        this.first = first;
        this.second = second;
    }
    public <T> void Swap(Pair<T> pair)
    {
        T temp = pair.first;
        pair.first = pair.second;
        pair.second = temp;
    }

    public T getFirst() {
        return first;
    }
    public T getSecond() {
        return second;
    }

    public void setFirst (T newValue) {
        first = newValue;
    }
    public void setSecond (T newValue) {
        second = newValue;
    }

    private T first;
    private T second;

}

