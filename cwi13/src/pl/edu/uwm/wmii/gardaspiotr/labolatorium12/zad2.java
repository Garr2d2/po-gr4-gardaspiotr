package pl.edu.uwm.wmii.gardaspiotr.labolatorium12;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class zad2 {

    static Map<String,String> StudentsDB = new HashMap<String,String>();

    void AddStudent(String Key, String Value)
    {
        StudentsDB.put(Key, Value);
    }
    void ShowSorted()
    {

        Object[] Array = StudentsDB.entrySet().toArray();
        Arrays.sort(Array, (a, b) -> a.toString().compareTo(b.toString()));

        for(Object x: Array)
        {
            System.out.println(x.toString().replace("=", ": "));
        }


    }
    void DeleteStudent(String Student)
    {

        StudentsDB.remove(Student);
    }
    void ChangeGrade(String Student, String NewGrade)
    {
        StudentsDB.replace(Student, NewGrade);
    }

    boolean Menu()
    {
        System.out.println("1.Dodaj studenta  2.Usun studenta 3.Zmien ocene studenta 4. Wypisz liste studentow   other. Exit");
        Scanner choice = new Scanner(System.in);
        switch(choice.nextInt()){
            case 1:
                AddStudent((choice.next()), choice.next()); //tylko nazwisko -> nie ma potrzeby wczytywać całej lini
                break;
            case 2:
                System.out.println("Type student name");
                DeleteStudent(choice.next());
                break;
            case 3:
                System.out.println("Type student name and new grade");
                ChangeGrade(choice.next(), choice.next());
                break;
            case 4:
                ShowSorted();
                break;
            default:
                System.out.println("exiting");
                return false;
        }
        return true;
    }

    public static void main(String[] args)
    {
        zad2 Students = new zad2();
        while(Students.Menu());
    }
}
