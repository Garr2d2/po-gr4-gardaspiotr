package pl.edu.uwm.wmii.gardaspiotr.labolatorium02;

import java.util.Arrays;
import java.util.Random;

public class Zad3 {
    public static void main(String[] args) {
        Zad3 fun = new Zad3();
        int[][] mat1 = new int[3][5];
        int[][] mat2 = new int[5][2];
        int[][] mat3 = new int[3][2];
        fun.WriteMatrix(mat1);
        fun.WriteMatrix(mat2);
        fun.WriteZeros(mat3);
        fun.Show(mat1);
        System.out.print("\n");
        fun.Show(mat2);
        fun.GetProduct(mat1, mat2, mat3, 3, 2, 5);
        System.out.print("\n");
        fun.Show(mat3);


    }

    public static void WriteMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = rand(0, 10);
            }
        }
    }

    public static void WriteZeros(int[][] matrix) {
        for (int[] ints : matrix) {
            Arrays.fill(ints, 0);
        }
    }


    private static int rand(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min) + min;
    }

    public static void Show(int[][] matrix) {
        for (int[] x : matrix) {
            for (int num : x) {
                System.out.print(num + " ");
            }
            System.out.print("\n");
        }
    }

    public static void GetProduct(int[][] matrixA, int[][] matrixB, int[][] result, int a, int b, int c) {
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                int temp = 0;
                for (int k = 0; k < c; k++) {
                    temp += matrixA[i][k] * matrixB[k][j];
                    result[i][j] = temp;
                }
            }
        }
    }
}