package pl.edu.uwm.wmii.gardaspiotr.labolatorium02;

import java.util.Random;
import java.util.Scanner;

public class Zad1_d {
    public static void main(String[] args) {
        Scanner Scan = new Scanner(System.in);
        Random Rand = new Random();
        int Positive =0;
        int Negative = 0;
        int n;
        do {
            System.out.println("wprowadz liczbe z przedzialu 0<n<101");
            n = Scan.nextInt();
        }
        while(n<1 | n>100);
        int Arr[] = new int[n];
        for(int i = 0; i<Arr.length; i++)
        {
            Arr[i] = (Math.abs((Rand.nextInt())% 1999) - 999);
        }

        for(int i = 0; i<Arr.length;i++)
        {
            if(Arr[i]>0)
            {
                Positive+=Arr[i];
            }
            else if(Arr[i]<0)
            {
                Negative+=Arr[i];
            }

        }
    }
}
