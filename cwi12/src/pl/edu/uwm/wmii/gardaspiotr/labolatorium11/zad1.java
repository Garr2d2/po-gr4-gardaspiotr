package pl.edu.uwm.wmii.gardaspiotr.labolatorium11;

import java.util.Arrays;
import java.util.LinkedList;

public class zad1 {
    public static void redukuj(LinkedList<String> pracownicy, int n)
    {
        if(n<=0 | n > pracownicy.size())
        {
            return;
        }
        for(int i = n-1; i<pracownicy.size(); i+=n-1)
        {
            pracownicy.remove(i);
        }
    }

    public static void main(String[] args) {
	LinkedList<String> pracownicy = new LinkedList<String>(Arrays.asList("prac1", "prac2", "prac3", "prac4","prac5", "prac6", "prac7", "prac8" ));
	for(String x : pracownicy)
    {
        System.out.println(x);
    }
	redukuj(pracownicy, 3);
        System.out.println();
	for(String x : pracownicy)
	{
	    System.out.println(x);
	}

    }
}
