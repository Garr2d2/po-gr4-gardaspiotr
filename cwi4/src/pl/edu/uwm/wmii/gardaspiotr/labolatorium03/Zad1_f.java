package pl.edu.uwm.wmii.gardaspiotr.labolatorium03;

import java.util.Scanner;
import java.lang.*;
public class Zad1_f {
    public static String change(String Str)
    {
        StringBuffer wynik = new StringBuffer();
        for (char c : Str.toCharArray())
        {
            wynik.append(Character.isUpperCase(c) ? Character.toLowerCase(c) : Character.toUpperCase(c));
        }
        return wynik.toString();
    }
    public static void main(String[] args) {
       // Scanner sc = new Scanner(System.in);
       // String s1 = new String(sc.next());
        System.out.println(change("JaNuSz"));
    }
}
