package pl.edu.uwm.wmii.gardaspiotr.labolatorium11;

import java.util.Arrays;
import java.util.LinkedList;

public class zad2 {
    public  static <T> void redukuj(LinkedList<T> pracownicy, int n)
    {
        if(n<=0 | n > pracownicy.size())
        {
            return;
        }
        for(int i = n-1; i<pracownicy.size(); i+=n-1)
        {
            pracownicy.remove(i);
        }
    }

    public static void main(String[] args) {
        LinkedList<Integer> pracownicy = new LinkedList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));
        for (Integer x : pracownicy) {
            System.out.println(x);
        }
        redukuj(pracownicy, 3);
        System.out.println();
        for (Integer x : pracownicy) {
            System.out.println(x);
        }
    }
}
