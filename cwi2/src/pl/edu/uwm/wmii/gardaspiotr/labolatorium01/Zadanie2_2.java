package pl.edu.uwm.wmii.gardaspiotr.labolatorium01;

import java.util.Scanner;

public class Zadanie2_2 {
    public static void main(String[] args) {

        Scanner Scn = new Scanner(System.in);
        System.out.println("enter number of cases");
        int n = Scn.nextInt();
        double Arr[] = new double[n];
        double sum = 0;
        for (int i = 0; i < n; i++) {
            System.out.print("enter next number: ");
            Arr[i] = Scn.nextDouble();
        }

        for(int i = 0; i<n ; i++)
        {

            if(Arr[i]>0)
            {
                sum+=Arr[i];
            }
        }
        sum*=2;
        System.out.println("result " + sum);
    }
}
