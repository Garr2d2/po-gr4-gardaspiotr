package pl.edu.uwm.wmii.gardaspiotr.labolatorium02;

import java.util.Random;

public class Zad2_f {
    public Zad2_f()
    {
        Rand = new Random();
    }
    private static Random Rand;

    public static void generuj(int Arr[], int n, int Min, int Max)
    {
        for(int i = 0; i<n; i++)
        {
            Arr[i] = (Math.abs((Rand.nextInt()%(Math.abs(Min)+Math.abs(Max)))))-Math.abs(Min);
        }
    }
    public static void Signum(int Arr[])
    {
        for(int i = 0; i<Arr.length; i++)
        {
            if(Arr[i]>0)
            {
                Arr[i] = 1;
            }
            else if(Arr[i]<0)
            {
                Arr[i]=-1;
            }
            if(i>0)
            {
                System.out.print(",");
            }
            System.out.print(Arr[i]);
        }
    }

    //////////////main
    public static void main(String[] args) {
        int n = 10;
        Zad2_f func = new Zad2_f();
        int Arr[] = new int[n];

        func.generuj(Arr, n, (-999), 999);

        func.Signum(Arr);



    }
}
