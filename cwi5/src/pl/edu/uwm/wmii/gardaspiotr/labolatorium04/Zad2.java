package pl.edu.uwm.wmii.gardaspiotr.labolatorium04;

import java.util.ArrayList;
import java.util.Arrays;

public class Zad2 {
    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b)
    {
        int n = a.size() > b.size() ? a.size() : b.size();
        ArrayList<Integer> result = new ArrayList<Integer>();
        for(int i =0; i<n;i++)
        {
            if(i < a.size())
            {
                result.add(a.get(i));
            }
            if(i < b.size())
            {
                result.add(b.get(i));
            }
        }


        return result;
    }
    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<Integer>(Arrays.asList(1,2,3,4));
        ArrayList<Integer> b = new ArrayList<Integer>(Arrays.asList(5,6,7,8,9,10,11,12));
        System.out.println(merge(a,b));
    }
}
