package pl.edu.uwm.wmii.gardaspiotr.labolatorium01;

import java.util.Scanner;

public class Zadanie2_1d {
    public static void main(String[] args) {
        Scanner Scn = new Scanner(System.in);
        System.out.println("enter number of cases");
        int n = Scn.nextInt();
        int Arr[] = new int[n];
        int count = 0;
        for (int i = 0; i < n; i++) {
            System.out.print("enter next number: ");
            Arr[i] = Scn.nextInt();
        }
        for(int i = 1; i< n - 1; ++i)
        {
            if(n<3)
            {
                break;
            }
            if(Arr[i] < ((Arr[i-1] + Arr[i+1])/2))
            {
                count++;
            }
        }
        System.out.println("result " + count);
    }
}
