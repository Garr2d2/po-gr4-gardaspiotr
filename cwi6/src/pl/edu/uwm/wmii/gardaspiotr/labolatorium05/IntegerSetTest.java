package pl.edu.uwm.wmii.gardaspiotr.labolatorium05;

public class IntegerSetTest {
    public static void main(String[] args) {
        IntegerSet one = new IntegerSet();
        IntegerSet two = new IntegerSet();
        System.out.println(one);
        System.out.println(two);
        System.out.println(one.equals(two));
        one.insertElement(20);
        two.insertElement(30);
        two.insertElement(20);
        System.out.println(one);
        System.out.println(two);

        System.out.println(one.equals(two));
        two.deleteElement(30);
        System.out.println(one);
        System.out.println(two);

        System.out.println(one.equals(two));
    }
}
