package pl.edu.uwm.wmii.gardaspiotr.labolatorium02;

import java.util.Random;

public class Zad2_c {
        public Zad2_c()
        {
            Rand = new Random();
        }
        private static Random Rand;

        public static void generuj(int Arr[], int n, int Min, int Max)
        {
            for(int i = 0; i<n; i++)
            {
                Arr[i] = (Math.abs((Rand.nextInt()%(Math.abs(Min)+Math.abs(Max)))))-Math.abs(Min);
            }
        }
        public static int IleMaksymalnych(int Arr[])
        {
            int Count = 0;
            int MaxTemp = Arr[0];
            for(int i = 0; i<Arr.length;i++)
            {
                if(Arr[i] == MaxTemp)
                {
                    Count++;
                }
                else if(Arr[i]>MaxTemp)
                {
                    Count = 1;
                    MaxTemp = Arr[i];
                }
            }
            return Count;
        }

        //////////////main
        public static void main(String[] args) {
            int n = 10;
            Zad2_c func = new Zad2_c();
            int Arr[] = new int[n];

            func.generuj(Arr, n, (-999), 999);
            int IleD = func.IleMaksymalnych(Arr);

            System.out.println(IleD);


        }
}
