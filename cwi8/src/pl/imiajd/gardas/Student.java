package pl.imiajd.gardas;

import pl.imiajd.gardas.Osoba;

import java.time.LocalDate;

public class Student extends Osoba {
    public Student(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec, String kierunek, double sredniaOcen) {
        super(nazwisko, imiona, dataUrodzenia, plec);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }

    public String getOpis() {
        StringBuffer Imiona = new StringBuffer();
        for (String x : getImiona()) {
            Imiona.append(x + ' ');
        }
        return String.format("pracownik %s %s plec %s  urodzony %s , srednia ocen %f",
                Imiona, getNazwisko(), getPlec() == true ? "mezczyzna" : "kobieta",
                getDataUrodzenia().toString(),
                getSredniaOcen());
    }

    public void setSredniaOcen(double newSrednia) {
        sredniaOcen = newSrednia;
    }

    public double getSredniaOcen() {
        return sredniaOcen;
    }

    private String kierunek;
    private double sredniaOcen;

}
