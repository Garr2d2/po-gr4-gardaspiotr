package pl.edu.uwm.wmii.gardaspiotr.labolatorium10.PairDemo1;

public class PairUtil<T> {
    static public <T> Pair<T> swap(Pair<T> pair) {
        Pair<T> result = new Pair<T>();
        result.setFirst(pair.getSecond());
        result.setSecond(pair.getFirst());
        return result;
    }
}
