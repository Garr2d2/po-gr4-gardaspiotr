package pl.imiajd.gardas;

import java.time.LocalDate;
import pl.imiajd.gardas.Instrument;

public class Fortepian extends Instrument {
    public Fortepian(String producent, int rokProdukcji) {
        super(producent, rokProdukcji);
    }

    public void dzwiek() {
        System.out.println("PlimPlamPlum");
    }
}
