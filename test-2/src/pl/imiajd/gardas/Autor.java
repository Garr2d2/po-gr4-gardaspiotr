package pl.imiajd.gardas;

public class Autor implements Cloneable, Comparable
{

   public Autor(String nazwa, String email, char plec)
    {
        this.nazwa = nazwa;
        this.email = email;
        this.plec = plec;
    }
    String nazwa;
    String email;
    char plec;
    void setNazwa(String nazwa) {this.nazwa = nazwa;}
    String getNazwa(){return nazwa;}

    void setEmail(String email) {this.email = email;}
    String getEmail(){return email;}

    void setPlec(char plec) {this.plec = plec;}
    char getPlec(){return plec;}

    @Override
    public int compareTo(Object o) {
        if(this.getNazwa().compareTo(((Autor)o).getNazwa()) != 0)
        {
            return this.getNazwa().compareTo(((Autor)o).getNazwa());
        }
        return (int)(this.getPlec() - ((Autor)o).getPlec());
    }

    @Override
    public String toString() {
        return "Autor [nazwisko = " + nazwa +", email = " +email+", plec = " + plec + "]";
    }




}

