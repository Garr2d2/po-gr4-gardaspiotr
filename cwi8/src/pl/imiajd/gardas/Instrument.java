package pl.imiajd.gardas;
import java.time.LocalDate;
public abstract class Instrument
{
    String producent;

    LocalDate rokProdukcji;

    public Instrument(String producent, int rokProdukcji)
    {
        this.producent = producent;
        this.rokProdukcji = LocalDate.of(rokProdukcji,1,1);
    }

    public String getProducent()
    {
        return producent;
    }

    public int getRokProdukcji()
    {
        return rokProdukcji.getYear();
    }

    public abstract void dzwiek();

    public String toString()
    {
        return producent + ' ' + rokProdukcji.getYear();
    }

    public boolean equals(Instrument a)
    {
        return (this.producent == a.getProducent() & this.rokProdukcji.getYear() == a.getRokProdukcji());
    }
}


