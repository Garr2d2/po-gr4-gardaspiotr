package pl.edu.uwm.wmii.gardaspiotr.labolatorium01;

import java.util.Scanner;

public class Zadanie2_3 {
    public static void main(String[] args) {

        Scanner Scn = new Scanner(System.in);
        System.out.println("enter number of cases");
        int n = Scn.nextInt();
        double Arr[] = new double[n];
        int positive = 0;
        int negative = 0;
        int zeros = 0;
        for (int i = 0; i < n; i++) {
            System.out.print("enter next number: ");
            Arr[i] = Scn.nextDouble();
        }

        for(int i = 0; i<n ; i++)
        {

            if(Arr[i]>0)
            {
                positive++;
            }
            else if(Arr[i]<0)
            {
                negative++;
            }
            else
            {
                zeros++;
            }
        }

        System.out.print("positive: " + positive +'\n'+
                         "negative: " + negative +'\n'+
                         "zeros: " + zeros +'\n'
        );
    }
}
