package pl.edu.uwm.wmii.gardaspiotr.labolatorium09;

import pl.imiajd.gardas.Student;


import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class Zadanie3 {
    public static void main(String[] args)
    {
        File file = new File(args[0]);
        ArrayList<String> Lines = new ArrayList<String>();
        try
        {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine())
            {
                Lines.add(scanner.next());
            }

        }
        catch(Exception e)
        {
            System.out.println("");
        }

        Lines.sort(String::compareTo);

        for(String x:Lines)
        {
            System.out.println(x);
        }
    }
}
