package pl.edu.uwm.wmii.gardaspiotr.labolatorium10.ArrayUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class ArrayUtil {

    public static <T extends Comparable<T>>  boolean isSorted(T[] arr)
    {
        ArrayList<T> cmp = new ArrayList<T>(Arrays.asList(arr));
        Collections.sort(cmp);
        for(int i = 0; i < arr.length; i++)
        {
            if(arr[i].compareTo(cmp.get(i)) != 0)
            {
                return false;
            }
        }
        return true;
    }

    public static <T extends Comparable<T>> int binSearch(T[] arr, T el)
    {
        for(int i = 0; i<arr.length; i++)
        {
            if(arr[i].equals(el))
            {
                return i;
            }
        }
        return -1;
    }

    public static <T extends Comparable<T>> void selectionSort(T[] arr)
    {
        for(int i = 0; i < arr.length; i++)
        {
            ///take min from whats left
            T min = arr[i];
            int indexOfMin = i;
            for(int j = i; j<arr.length;j++)
            {
                if(arr[j].compareTo(min)<0)
                {
                    min = arr[j];
                    indexOfMin = j;
                }
            }
            ///swap places if necessary
            if(indexOfMin != i)
            {
                T temp = arr[i];
                arr[i] = min;
                arr[indexOfMin] = temp;
            }
        }
    }
    /////////////////////////////////////////merge sort///////////////////////////////////////////////
    public static  <T extends Comparable<T>> void mergeSort(T[] array)
    {
        mergeSort(array, 0, array.length-1);
    }

    static  <T extends Comparable<T>> void mergeSort(T[] array, int start, int end)
    {

        if (start < end)
        {

            int mid = (start + end) / 2;

            mergeSort(array, start, mid); // sort first half
            mergeSort(array, mid + 1, end);  // sort second half


            merge(array, start, mid, end);
        }
    }


    static <T extends Comparable<T>> void merge(T[] arr, int start, int mid, int end)
    {
        T[] lArr  = (T[]) new Comparable[mid - start + 1];
        T[] rArr = (T[]) new Comparable[end - mid];


        for (int i = 0; i < lArr.length; ++i)
            lArr[i] = arr[start + i];


        for (int i = 0; i < rArr.length; ++i)
            rArr[i] = arr[mid + 1 + i];

        int lInd = 0, rInd = 0;

        int currInd = start;

        while (lInd < lArr.length && rInd < rArr.length)
        {
            if (lArr[lInd].compareTo(rArr[rInd]) <= 0)
            {
                arr[currInd] = lArr[lInd];
                lInd++;
            }
            else
            {
                arr[currInd] = rArr[rInd];
                rInd++;
            }
            currInd++;
        }

        while (lInd < lArr.length) arr[currInd++] = lArr[lInd++];

        while (rInd < rArr.length) arr[currInd++] = rArr[rInd++];
    }
}
