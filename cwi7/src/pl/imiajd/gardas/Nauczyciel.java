package pl.imiajd.gardas;

public class Nauczyciel extends Osoba {
    private int pensja  = 0;

    public Nauczyciel(String nazw, String rok, int pen)
    {
        super(nazw,rok);
        pensja = pen;
    }
    public int getPensja() {return pensja;}
    public String toString()
    {
        return (String)(super.toString() + " " + "Pensja: " + Integer.toString(pensja));
    }
}
