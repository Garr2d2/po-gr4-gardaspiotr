package pl.edu.uwm.wmii.gardaspiotr.labolatorium07;

import pl.imiajd.gardas.Adres;

public class TestAdres {
    public static void main(String[] args) {
        Adres yeden = new Adres("ulica1", "21", "olsztyn", "10-900");
        Adres dfa = new Adres("ulica1", "21", "olsztyn", "10-911", "15");
        yeden.Pokaz();
        dfa.Pokaz();
        System.out.println(yeden.Przed(dfa));
    }
}
