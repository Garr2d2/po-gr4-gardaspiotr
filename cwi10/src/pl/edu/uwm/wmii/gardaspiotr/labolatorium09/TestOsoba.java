package pl.edu.uwm.wmii.gardaspiotr.labolatorium09;

import pl.imiajd.gardas.Osoba;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;

public class TestOsoba {
    public static void main(String[] args) {
        ArrayList<Osoba> grupa = new ArrayList<Osoba>(Arrays.asList(new Osoba("Gardas", LocalDate.of(1996,12,2)),
                new Osoba("Jasiewicz", LocalDate.of(1999,2,2)),
                new Osoba("Adasiewicz", LocalDate.of(1999,2,2)),
                new Osoba("Adasiewicz", LocalDate.of(1998,2,2)),
                new Osoba("Malgosiewicz", LocalDate.of(1999,2,2))));

        for(Osoba x : grupa)
        {
            System.out.println(x.toString());
        }

        grupa.sort(Osoba::compareTo);
        System.out.println();

        for(Osoba x : grupa)
        {
            System.out.println(x.toString());
        }

    }
}
