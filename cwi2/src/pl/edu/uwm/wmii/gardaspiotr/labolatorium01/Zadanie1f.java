package pl.edu.uwm.wmii.gardaspiotr.labolatorium01;

import java.util.Scanner;

public class Zadanie1f {
    public static void main(String[] args) {
        Scanner Scn = new Scanner(System.in);
        System.out.println("enter number of cases");
        int n = Scn.nextInt();
        double result = 0;
        for (int i =0; i<n; i++)
        {
            double x;
            System.out.print("enter next number: ");
            x = Scn.nextInt();
            result+=Math.pow(x,2);
        }
        System.out.println("result " + result);
    }
}
