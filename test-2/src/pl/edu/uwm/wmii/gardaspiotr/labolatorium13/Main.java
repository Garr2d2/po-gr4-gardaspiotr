package pl.edu.uwm.wmii.gardaspiotr.labolatorium13;

import pl.imiajd.gardas.Autor;
import pl.imiajd.gardas.Ksiazka;

import java.util.ArrayList;


public class Main {

    public static void main(String[] args)
    {
        ArrayList<Autor> AArr = new ArrayList<>();
        AArr.add(new Autor("jaś", "email2", 'm'));
        AArr.add(new Autor("małgosia", "email3", 'k'));
        AArr.add(new Autor("ela", "email4", 'k'));
        AArr.add(new Autor("jaś", "email1", 'm'));

        for(Autor x: AArr)
        {
            System.out.println(x.toString());
        }
        System.out.println();

        AArr.sort(Autor::compareTo);

        for(Autor x: AArr)
        {
            System.out.println(x.toString());
        }
        System.out.println();

        ArrayList<Ksiazka> KArr = new ArrayList<>();
        KArr.add(new Ksiazka("jaś", "email1", 'm', "tytul2",5.99));
        KArr.add(new Ksiazka("jaś", "email1", 'm', "tytul1",8.99));
        KArr.add(new Ksiazka("malgosia", "email2", 'k', "tytul10",2.99));
        KArr.add(new Ksiazka("karolina", "email4", 'k', "tytul11",5.99));

        for(Ksiazka x: KArr)
        {
            System.out.println(x.toString());
        }

        System.out.println();

        KArr.sort(Ksiazka::compareTo);

        for(Ksiazka x: KArr)
        {
            System.out.println(x.toString());
        }
    }

}