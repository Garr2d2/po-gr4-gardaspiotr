package pl.edu.uwm.wmii.gardaspiotr.labolatorium02;

import java.util.Random;

public class Zad2_e {
    public Zad2_e()
    {
        Rand = new Random();
    }
    private static Random Rand;

    public static void generuj(int Arr[], int n, int Min, int Max)
    {
        for(int i = 0; i<n; i++)
        {
            Arr[i] = (Math.abs((Rand.nextInt()%(Math.abs(Min)+Math.abs(Max)))))-Math.abs(Min);
        }
    }
    public static int DluoscMaksymalnegoCiaguDodatnich(int Arr[])
    {
        int SeriesCount = 0;
        int TempCount = 0;
        for(int i = 0; i<Arr.length;i++)
        {
            if(Arr[i]>0)
            {
                TempCount++;
            }
            else
            {
                if(TempCount>SeriesCount)
                {
                    SeriesCount = TempCount;
                }
                TempCount=0;
            }
        }
        if(TempCount>SeriesCount)
        {
            SeriesCount = TempCount;
        }
        return SeriesCount;
    }
    
    //////////////main
    public static void main(String[] args) {
        int n = 10;
        Zad2_e func = new Zad2_e();
        int Arr[] = new int[n];

        func.generuj(Arr, n, (-999), 999);
        
        int dlg = func.DluoscMaksymalnegoCiaguDodatnich(Arr);
        System.out.println(dlg);


    }
}
