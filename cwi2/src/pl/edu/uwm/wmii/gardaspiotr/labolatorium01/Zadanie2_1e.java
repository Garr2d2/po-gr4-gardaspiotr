package pl.edu.uwm.wmii.gardaspiotr.labolatorium01;

import java.util.Scanner;


public class Zadanie2_1e {

    public static void main(String[] args) {
        Scanner Scn = new Scanner(System.in);
        System.out.println("enter number of cases");
        int n = Scn.nextInt();
        int Arr[] = new int[n];
        int count = 0;
        for (int i = 0; i < n; i++) {
            System.out.print("enter next number: ");
            Arr[i] = Scn.nextInt();
        }
        int fac = 1;
       for(int i = 1; i<=n ; i++)
       {
           fac *=i;
           if(Math.pow(2,i) < Arr[i-1] && Arr[i-1] < fac)
           {
               count++;
           }
       }
        System.out.println("result " + count);
    }

}
