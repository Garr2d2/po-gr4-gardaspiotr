package pl.edu.uwm.wmii.gardaspiotr.labolatorium07;


import pl.imiajd.gardas.Nauczyciel;
import pl.imiajd.gardas.Osoba;
import pl.imiajd.gardas.Student;

public class TestOsoba {
    public static void main(String[] args)
    {
        Student jeden = new Student("nowak", "1996", "teologia");
        Nauczyciel dwa = new Nauczyciel("kowalski", "1986", 17*160);
        Osoba trzy = new Osoba("niekonieczny", "1999");
        System.out.println(jeden.toString());
        System.out.println(dwa.toString());
        System.out.println(trzy.toString());
        System.out.println(jeden.getNazwisko() + " " + jeden.getRokUrodzenia() + " " + jeden.getKierunek());
    }
}
