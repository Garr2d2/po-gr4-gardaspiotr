package pl.edu.uwm.wmii.gardaspiotr.labolatorium01;

import java.util.Scanner;

public class Zadanie2_1b {
    public static void main(String[] args) {
        Scanner Scn = new Scanner(System.in);
        System.out.println("enter number of cases");
        int n = Scn.nextInt();
        int count = 0;
        for (int i = 0; i < n; i++) {
            System.out.print("enter next number: ");
            int x = Scn.nextInt();
            if (x % 3 == 0 && x%5 != 0) {
                count++;
            }

        }
        System.out.println("result " + count);
    }
}
