package pl.edu.uwm.wmii.gardaspiotr.labolatorium04;

import java.util.ArrayList;
import java.util.Arrays;

public class Zad3 {
    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b)
    {
        int sizeA = a.size();
        int sizeB = b.size();
        ArrayList<Integer> result = new ArrayList<Integer>();
        for(int i =0, j=0; i+j < sizeA+sizeB;) //adding elements
        {
            if(i < sizeA & j < sizeB)
            {
                if(a.get(i) < b.get(j))
                {
                    result.add(a.get(i));
                    i++;
                }
                else
                {
                    result.add(b.get(j));
                    j++;
                }
            }
            else
            {
                if(i<sizeA)
                {
                    result.add(a.get(i));
                    i++;
                }
                else
                {
                    result.add(b.get(j));
                    j++;
                }
            }
        }


        return result;
    }
    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<Integer>(Arrays.asList(1,2,3,3,4,6));
        ArrayList<Integer> b = new ArrayList<Integer>(Arrays.asList(1,2,2,3,3,5,6,7,8,9,10,11,12));
        System.out.println(mergeSorted(a,b));
    }
}
