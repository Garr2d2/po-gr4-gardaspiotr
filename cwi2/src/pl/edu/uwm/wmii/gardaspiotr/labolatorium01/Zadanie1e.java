package pl.edu.uwm.wmii.gardaspiotr.labolatorium01;

import java.util.Scanner;
import java.lang.Math;

public class Zadanie1e {
    public static void main(String[] args) {
        Scanner Scn = new Scanner(System.in);
        System.out.println("enter number of cases");
        int n = Scn.nextInt();
        int Arr[] = new int[n]; //tylko dlatego ze w poleceniu bylo "wczytaj"
        double result = 1;
        for (int i =0; i<n; i++)
        {
            double x;
            System.out.print("enter next number: ");
            x = Scn.nextInt();
            result*=Math.abs(x);
        }
        if(n != 0)
        {
            System.out.println("result " + result);
        }
        else
        {
            System.out.println("result " + 0);
        }

    }
}
