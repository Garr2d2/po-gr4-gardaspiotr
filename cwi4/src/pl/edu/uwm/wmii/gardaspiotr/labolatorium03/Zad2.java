package pl.edu.uwm.wmii.gardaspiotr.labolatorium03;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Zad2 {
    public static int get(String name, char c)
    {
        int count = 0;
        try {
            File myObj = new File(name);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                for(char x: data.toCharArray())
                {
                    if(x == c)
                    {
                        count++;
                    }
                }
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return count;
    }
    public static void main(String[] args) {
        System.out.print(get("E:\\Studia2r\\ProjectsPo\\cwi4\\src\\pl\\edu\\uwm\\wmii\\gardaspiotr\\labolatorium03\\text.txt", 'A'));
    }
}
