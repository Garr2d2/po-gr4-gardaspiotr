package pl.edu.uwm.wmii.gardaspiotr.labolatorium12;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.TreeMap;


public class zad4 {

    public static void main(String[] args) throws FileNotFoundException {

        Map<Integer, HashSet<String>> map2 = new HashMap();
        Map<Integer, Integer> map = new HashMap();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("E:\\Studia2r\\ProjectsPo\\cwi13\\loreipsum.txt"))) {
            StringBuilder stringBuilder = new StringBuilder();
            String line = bufferedReader.readLine();

            while (line != null) {
                String[] words = line.split(" ");
                for (int i = 0; i < words.length; i++) {
                    if (map2.get(words[i].hashCode()) == null) {
                        HashSet<String> pomoc = new HashSet<>();
                        pomoc.add(words[i]);
                        map2.put(words[i].hashCode(), pomoc);
                        map.put(words[i].hashCode(), 1);
                    } else {
                        int newValue = Integer.parseUnsignedInt(String.valueOf(map.get(words[i].hashCode())));
                        newValue++;
                        map.put(words[i].hashCode(), newValue);
                    }
                }
                stringBuilder.append(System.lineSeparator());
                line = bufferedReader.readLine();
            }
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        Map<Integer, HashSet<String>> sorted = new TreeMap<Integer, HashSet<String>>(map2);
        for (Object x : sorted.keySet()) {
            if(map.get(x) > 1)
            {
                System.out.println("Word: " + map2.get(x) + " Counts: " + map.get(x));
            }
        }
    }
}