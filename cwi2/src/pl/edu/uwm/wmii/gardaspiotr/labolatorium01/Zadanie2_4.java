package pl.edu.uwm.wmii.gardaspiotr.labolatorium01;

import java.util.Scanner;
import java.util.*;

public class Zadanie2_4 {
    public static void main(String[] args) {

        Scanner Scn = new Scanner(System.in);
        System.out.println("enter number of cases");
        int n = Scn.nextInt();
        List<Double> Lista = new ArrayList<Double>();
        for (int i = 0; i < n; i++) {
            System.out.print("enter next number: ");
            Lista.add(Scn.nextDouble());
        }
        Collections.sort(Lista);
        double MinVal = Lista.get(0);
        double MaxVal = Lista.get(Lista.size()-1);


        System.out.print("MinVal: " + MinVal +'\n'+
                "MaxVal: " + MaxVal +'\n'
        );
    }
}
