package pl.imiajd.gardas;

public class Student  extends Osoba {
    private String kierunek  = null;

    public Student(String nazw, String rok, String kier)
    {
        super(nazw,rok);
        kierunek = kier;
    }
    public String getKierunek() {return kierunek;}
    public String toString()
    {
        return (String)(super.toString() + " " + "Kierunek: " + kierunek);
    }

}
