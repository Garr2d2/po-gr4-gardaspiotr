package pl.edu.uwm.wmii.gardaspiotr.labolatorium11;


import java.util.Stack;

public class zad5 {


    public static void main(String[] args) {



        Stack<Stack<String>> stack = new Stack<Stack<String>>();
        stack.push(new Stack<String>());
        String s1 = new String("Ala ma kota. Jej kot lubi myszy.");
        String[] words = s1.split(" ");

        for(String x:words)
        {
            if(x.toCharArray()[x.length()-1]!='.')
            {
                if(stack.lastElement().empty())
                {
                    stack.lastElement().push(x.toLowerCase() + ". ");
                }
                else
                {
                    stack.lastElement().push(x.toLowerCase() + " ");

                }
            }
            else
            {
                stack.lastElement().push(x.substring(0, 1).toUpperCase() + (x.length() > 1 ? x.substring(1,x.length()-1) + " " : " "));
                stack.push(new Stack<String>());
            }
        }

        while(!stack.empty())
        {
            while(!stack.firstElement().empty())
            {
                System.out.print(stack.firstElement().pop());
            }
            stack.remove(0);
        }
    }
}
