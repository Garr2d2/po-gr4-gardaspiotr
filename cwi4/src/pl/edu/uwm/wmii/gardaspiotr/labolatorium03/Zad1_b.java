package pl.edu.uwm.wmii.gardaspiotr.labolatorium03;

public class Zad1_b {
    public static int CountSubStr(String Str, String SubString)
    {
        int Count = 0;
        int Index = 0;
        while((Index = Str.indexOf(SubString, Index))!= -1)
        {
            Count++;
            Index++;
        }
        return Count;
    }
    public static void main(String[] args) {

        String s1 = new String("Janusz byl w Januszowie Na Zlocie Januszy");
        String SubStr = new String("Janusz");
        System.out.println("ilosc wystapien frazy: " + SubStr +" w str: " + s1 + " to: " +CountSubStr(s1, SubStr));
    }
}
