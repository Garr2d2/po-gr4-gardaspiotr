package pl.edu.uwm.wmii.gardaspiotr.labolatorium03;

import java.math.BigDecimal;

import java.math.RoundingMode;

public class Zad5 {
    public static BigDecimal function(double k, double p, double n)
    {
        BigDecimal result = new BigDecimal(Double.toString(k));
        for(int i = 0; i<n;i++)
        {
            result = result.add((result.multiply(BigDecimal.valueOf(p))).divide(BigDecimal.valueOf(100)));
        }
        return result.setScale(2, RoundingMode.HALF_UP);



    }
    public static void main(String[] args) {
        System.out.println(function(1000, 10, 3));

    }
}
