package pl.edu.uwm.wmii.gardaspiotr.labolatorium07;
import pl.imiajd.gardas.Osoba;
import pl.imiajd.gardas.Pracownik;
import pl.imiajd.gardas.Student;

import java.time.LocalDate;

public class TestOsoba// w celu sprawdzenia czy nowe metody działają poprawnie zmodyfikowałem metodę getOpis()
{
    public static void main(String[] args)
    {
        Osoba[] ludzie = new Osoba[2];

        ludzie[0] = new Pracownik("Jan", new String[]{"adam", "andrzej"},LocalDate.of(1999,12,30),true,50000, LocalDate.of(2010,12,30));
        ludzie[1] = new Student("Nowak", new String[] {"Małgorzata"}, LocalDate.of(1998,12,2), false, "informatyka", 5.0);
        // ludzie[2] = new Osoba("Dyl Sowizdrzał");

        for (Osoba p : ludzie) {
            System.out.println(p.getOpis());
        }

        Student malgosia = new Student("Nowak", new String[] {"Małgorzata"}, LocalDate.of(1998,12,2), false, "informatyka", 5.0);
        malgosia.setSredniaOcen(4.00);
        System.out.println(malgosia.getOpis());
    }
}


