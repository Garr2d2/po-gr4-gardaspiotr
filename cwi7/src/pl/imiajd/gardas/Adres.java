package pl.imiajd.gardas;

public class Adres {
private String ulica = null;
private String numer_domu = null;
private String numer_mieszkania = null;
private String miasto = null;
private String kod_pocztowy = null;

public Adres(String ulica, String numer_domu, String miasto, String kod_pocztowy)
{
    this.ulica = ulica;
    this.numer_domu = numer_domu;
    this.miasto = miasto;
    this.kod_pocztowy = kod_pocztowy;
}

public Adres(String ulica, String numer_domu, String miasto, String kod_pocztowy, String numer_mieszkania)
{
    this.ulica = ulica;
    this.numer_domu = numer_domu;
    this.miasto = miasto;
    this.kod_pocztowy = kod_pocztowy;
    this.numer_mieszkania = numer_mieszkania;
}

public void Pokaz()
{
    System.out.println(kod_pocztowy + ' ' + miasto);
    System.out.print(ulica + " " + numer_domu + (numer_mieszkania != null ? ' ' + numer_mieszkania + "\n" : "\n") );
}
public boolean Przed(Adres a)
{
    return kod_pocztowy.compareToIgnoreCase(a.kod_pocztowy) < 0 ? true : false;
}

}
