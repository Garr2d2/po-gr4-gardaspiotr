package pl.edu.uwm.wmii.gardaspiotr.labolatorium03;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Zad1_e {
    public static int[] where(String Str, String SubStr)
    {
        List<Integer> list = new ArrayList<Integer>();
        int index = 0;
        while((index = Str.indexOf(SubStr, index)) != -1)
        {
            list.add(index);
            index++;
        }
        int arr[] = new int[list.size()];
        for(int i =0; i< list.size();i++)
        {
            arr[i] = list.get(i);
        }
        return arr;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s1 = new String(sc.nextLine());
        String s2 = new String(sc.next());
        List<Integer> list = Arrays.stream(where(s1,s2))
                .boxed()
                .collect(Collectors.toList());
        for(int i:list)
        {
            System.out.print(" " + i );
        }
    }
}
