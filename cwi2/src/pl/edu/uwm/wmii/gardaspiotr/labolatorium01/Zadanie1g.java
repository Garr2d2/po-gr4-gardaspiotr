package pl.edu.uwm.wmii.gardaspiotr.labolatorium01;

import java.util.Scanner;

public class Zadanie1g {
    double result1 = 0;
    double result2 = 1;
    public void r1(double Arr[]) {
        for (double x:Arr)
        {
            result1+=x;
        }
        System.out.println("result " + result1);
    }
    public void r2(double Arr[]) {
        if(Arr.length == 0)
        {
            System.out.println("result " + 0);
            return;
        }
        for (double x:Arr)
        {
            result2*=x;
        }
        System.out.println("result " + result2);
    }

    public static void main(String[] args) {
        Zadanie1g cls = new Zadanie1g();
        Scanner Scn = new Scanner(System.in);
        System.out.println("enter number of cases");
        int n = Scn.nextInt();
        double Arr[] = new double[n];

        for (int i=0; i<Arr.length;i++)
        {
            System.out.print("enter next number: ");
            Arr[i] = Scn.nextDouble();

        }

        cls.r1(Arr);
        cls.r2(Arr);
    }
}
