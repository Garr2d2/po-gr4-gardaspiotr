package pl.imiajd.gardas;

import java.time.LocalDate;

public class Student extends Osoba implements Cloneable, Comparable
{
    private double sredniaOcen;

    public Student(String nazwisko, LocalDate dataUrodzenia, double sredniaOcen)
    {
        super(nazwisko, dataUrodzenia);
        this.sredniaOcen = sredniaOcen;
    }

    @Override
    public int compareTo(Object o) {
        return super.compareTo(o) != 0 ? super.compareTo(o) :
                (int)java.lang.Math.signum(this.sredniaOcen - ((Student)o).sredniaOcen);//means lower average higher in list
    }

    @Override
    public String toString() {
        return super.toString() +", " + '[' + sredniaOcen + ']';
    }
}
