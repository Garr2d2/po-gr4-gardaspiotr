package pl.edu.uwm.wmii.gardaspiotr.labolatorium12;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class zad3 {
    static Map<Student,String> StudentsDB = new HashMap<Student,String>();

    void AddStudent(Student Key, String Value)
    {
        StudentsDB.put(Key, Value);
    }
    void ShowSorted()
    {

        Object[] Array = StudentsDB.entrySet().toArray();
        Arrays.sort(Array, (a, b) -> a.toString().compareTo(b.toString()));


        for(Object x: Array)
        {
            System.out.println(x.toString().replace("=", ": "));
        }


    }
    void DeleteStudent(int ID)
    {

        for (Student key : StudentsDB.keySet())
        {
            if(key.GetID() == ID)
            {
                StudentsDB.remove(key);
                break;
            }
        }
    }

    void ChangeGrade(int ID, String NewGrade)
    {
        for (Student key : StudentsDB.keySet())
        {
            if(key.GetID() == ID)
            {
                StudentsDB.replace(key, NewGrade);
                break;
            }
        }
    }

    /*boolean Menu()
    {
        System.out.println("1.Dodaj studenta  2.Usun studenta 3.Zmien ocene studenta 4. Wypisz liste studentow   other. Exit");
        Scanner choice = new Scanner(System.in);
        switch(choice.nextInt()){
            case 1:
                AddStudent((choice.next()), choice.next()); //tylko nazwisko -> nie ma potrzeby wczytywać całej lini
                break;
            case 2:
                System.out.println("Type student name");
                DeleteStudent(choice.next());
                break;
            case 3:
                System.out.println("Type student name and new grade");
                ChangeGrade(choice.next(), choice.next());
                break;
            case 4:
                ShowSorted();
                break;
            default:
                System.out.println("exiting");
                return false;
        }
        return true;
    }*/

    public static void main(String[] args)
    {
        zad3 Students = new zad3();
        Students.AddStudent(new Student("bbb", "CCC"), "DB+");
        Students.AddStudent(new Student("aaa", "CCC"), "DB+");
        Students.AddStudent(new Student("kkk", "GGG"), "DB+");
        Students.AddStudent(new Student("SSS", "DDD"), "DB+");
        Students.AddStudent(new Student("DDD", "DDD"), "DB+");

        Students.ShowSorted();
        System.out.println("\n");

        Students.DeleteStudent(3);

        Students.ShowSorted();
        System.out.println("\n");

        Students.ChangeGrade(0, "BDB");

        Students.ShowSorted();
    }
}




class Student
{
    static int StudentsCount = 0;
    String Imie;
    String Nazwisko;
    int ID;
    Student(String Imie, String Nazwisko)
    {
        this.Imie = Imie;
        this.Nazwisko = Nazwisko;
        ID = StudentsCount;
        StudentsCount++;
    }
    String GetImie(){return Imie;}
    String GetNazwisko(){return Nazwisko;}
    int GetID(){return ID;}

    @Override
    public String toString() {
        return "Nazwisko: " + Nazwisko + ", Imie: " + Imie + ", Id: " + ID;
    }


}
