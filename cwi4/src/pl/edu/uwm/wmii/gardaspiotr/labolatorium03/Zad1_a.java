package pl.edu.uwm.wmii.gardaspiotr.labolatorium03;

public class Zad1_a {
    public static int CountChar(String str, char c) // zakladam, ze wielkolsc liter nie gra roli
    {
        int count =0;
        String s1 = Character.toString(c);
        c = ((s1.toUpperCase()).toCharArray())[0];
        str = str.toUpperCase();
        for(char s :str.toCharArray())
        {
            if(s==c)
            {
                count++;
            }
        }
        return count;
    }
    public static void main(String[] args) {
        char c = 'a';
	String s1 = new String("ANASTAzja");
	System.out.println("ilosc wystapien znaku: " + c +" w str: " + s1 + " to: " +CountChar(s1, c));
    }
}
