package pl.edu.uwm.wmii.gardaspiotr.labolatorium01;

import java.util.Scanner;

public class Zadanie1b {
    public static void main(String[] args) {
        Scanner Scn = new Scanner(System.in);
        System.out.println("enter number of cases");
        int n = Scn.nextInt();
        double res = 1;
        for (int i =0; i<n; i++)
        {
            double x;
            System.out.print("enter next number: ");
            x = Scn.nextDouble();
            res*=x;
        }
        System.out.println("result " + (n!=0 ? res : 0));
        
    }
}
