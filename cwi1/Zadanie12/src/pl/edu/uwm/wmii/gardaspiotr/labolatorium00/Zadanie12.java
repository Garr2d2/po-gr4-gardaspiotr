package pl.edu.uwm.wmii.gardaspiotr.labolatorium00;

public class Zadanie12 {

    public static void main(String[] args)
    {
        boolean bIsRed = true;
        for (int i = 0; i <13 ; i++)
        {
            if(i>0 && i<12)
            {
                System.out.print('|');
            }
            else
            {
                System.out.print(' ');
            }


            for (int j = 0; j < 54; j++)
            {
                if(i<6 && j <20)
                {
                    if(j%2 == 0)
                    {
                        System.out.print(' ');
                    }
                    else
                    {
                        System.out.print('*');
                    }
                }
                else
                {
                    if(bIsRed)
                    {
                        System.out.print('-');
                    }
                    else
                    {
                        System.out.print(' ');
                    }
                }

            }
            if(i>0 && i < 12)
            {
                System.out.print('|');
            }
            bIsRed=!bIsRed;
            System.out.println();

        }
    }
}
