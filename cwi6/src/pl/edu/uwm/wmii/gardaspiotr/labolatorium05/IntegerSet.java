package pl.edu.uwm.wmii.gardaspiotr.labolatorium05;

public class IntegerSet {
    private int n = 100;
    private boolean [] set = new boolean[n];

    IntegerSet()
    {
        for(int i = 0; i<100; i++)
        {
            set[i] = false;
        }
    }
    public static IntegerSet union(IntegerSet a, IntegerSet b)
    {
        IntegerSet wynik = new IntegerSet();
        for(int i = 0; i < wynik.n; i++)
        {
            if(a.set[i] == true | b.set[i] == true)
            {
                wynik.set[i] = true;
            }
        }
        return wynik;
    }
    public static IntegerSet intersection(IntegerSet a, IntegerSet b)
    {
        IntegerSet wynik = new IntegerSet();
        for(int i = 0; i < wynik.n; i++)
        {
            if(a.set[i] == true & b.set[i] == true)
            {
                wynik.set[i] = true;
            }
        }
        return wynik;
    }
    public void insertElement(int x)
    {
        if(x<=0 | x>=this.n) //for safety
        {
            return;
        }
        this.set[x-1] = true;
    }
    public void deleteElement(int x)
    {
        if(x<=0 | x>=this.n) //for safety
        {
            return;
        }
        this.set[x-1] = false;
    }
    public String toString()
    {
        StringBuffer wynik = new StringBuffer();
        for(int i = 0; i < n; i++)
        {
            if(set[i] == true)
            {
                wynik.append(i+1);
                wynik.append(' ');
            }
        }
        return wynik.toString();
    }

    public boolean equals(IntegerSet a)
    {
        for(int i = 0; i< n; i++)
        {
            if(set[i] != a.set[i])
            {
                return false;
            }
        }
        return true;
    }

}
