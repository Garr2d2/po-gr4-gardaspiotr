package pl.edu.uwm.wmii.gardaspiotr.labolatorium07;
import pl.imiajd.gardas.Instrument;
import java.util.ArrayList;
import pl.imiajd.gardas.Fortepian;
import pl.imiajd.gardas.Skrzypce;
import pl.imiajd.gardas.Flet;

import java.time.LocalDate;
public class TestInstrument {

    public static void main(String[] args) {

	    ArrayList<Instrument> Orkiestra = new ArrayList<Instrument>();
	    Orkiestra.add(new Fortepian("producen1t", 1800));
	    Orkiestra.add(new Skrzypce("producent2", 1802));
	    Orkiestra.add(new Flet("producent", 2020));
	    Orkiestra.add(new Flet("producent", 2020));
	    Orkiestra.add(new Flet("producent",2020));

	    for(Instrument x : Orkiestra)
        {
            x.dzwiek();
        }

        for(Instrument x : Orkiestra)
        {
            System.out.println(x.getClass().getSimpleName() + ' ' +  x.toString());
        }
    }

}
