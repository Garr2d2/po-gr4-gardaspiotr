package pl.edu.uwm.wmii.gardaspiotr.labolatorium04;

import java.util.ArrayList;
import java.util.Arrays;

public class Zad1 {
    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b)
    {
        ArrayList<Integer> result = new ArrayList<Integer>(a);
        result.addAll(b);
        return result;
    }
    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<Integer>(Arrays.asList(1,2,3,4));
        ArrayList<Integer> b = new ArrayList<Integer>(Arrays.asList(5,6,7,8,9));
        System.out.println(append(a,b));
    }
}
